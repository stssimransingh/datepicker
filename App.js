import React from 'react';
import {View, Text} from 'react-native';
import firebase from 'react-native-firebase';
import DatePicker from 'react-native-datepicker';
class App extends React.Component{
  constructor(){
    super();
    this.state = {date: new Date()}
    let d = new Date();
    // 1567416090841
    this.date = d.getHours() +':'+ d.getMinutes() +':'+ d.getSeconds();
  }

  render(){
    return(
      <View style={{flex:1}}>
        <Text>{this.date}</Text>
        <DatePicker 
          date={this.state.date}
          mode="date"
          minDate="2016-05-14"
          maxDate="2017-06-01"
          showIcon={false}
          hideText={false}
          customStyles={{
            dateIcon: {
              position: 'absolute',
              left: 0,
              top: 4,
              marginLeft: 0
            },
            dateInput: {
              marginLeft: 36
            }
            // ... You can check the source to find the other keys.
          }}
          onDateChange={(date) => {this.setState({date: date})}}
        />
      </View>
    )
  }
}

export default App;